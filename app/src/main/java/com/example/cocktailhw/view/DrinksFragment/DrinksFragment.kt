package com.example.cocktailhw.view.DrinksFragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.cocktailhw.databinding.FragmentDrinksBinding
import com.example.cocktailhw.view.CategoryFragment.CategoryAdapter
import com.example.cocktailhw.viewmodel.DrinksViewModel

class DrinksFragment : Fragment() {

    lateinit var binding: FragmentDrinksBinding
    private val args by navArgs<DrinksFragmentArgs>()
    val viewModel by viewModels<DrinksViewModel>()
    private val adapter by lazy {
        DrinksAdapter()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentDrinksBinding.inflate(layoutInflater)
        viewModel.getDrinks(args.category)
        // adapter set up
        binding.recyclerView.adapter = adapter
        binding.recyclerView.layoutManager = LinearLayoutManager(this@DrinksFragment.context)
        initObservers()
        return binding.root
    }

    private fun initObservers() {
        viewModel.drinksState.observe(viewLifecycleOwner) {
            adapter.setList(it.drinks)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        binding.btnGoBack.setOnClickListener {
            val action = DrinksFragmentDirections.actionDrinksFragment2ToCategoryFragment()
            findNavController().navigate(action)
        }
    }


}