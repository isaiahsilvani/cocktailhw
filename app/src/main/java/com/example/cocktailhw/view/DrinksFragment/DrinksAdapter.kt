package com.example.cocktailhw.view.DrinksFragment

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import coil.load
import com.example.cocktailhw.databinding.ItemDrinkViewBinding
import com.example.cocktailhw.model.models.DrinkDetails

class DrinksAdapter : RecyclerView.Adapter<DrinksAdapter.DrinkViewHolder>() {

    var drinkList: List<DrinkDetails> = listOf()

    inner class DrinkViewHolder(val binding: ItemDrinkViewBinding) :
        RecyclerView.ViewHolder(binding.root) {
        var drinkName: TextView = binding.tvDrink
        var drinkImg: ImageView = binding.imgDrink
        var wholeItem: ConstraintLayout = binding.layoutItem
        fun displayImage(image: String) {
            binding.imgDrink.load(image)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DrinkViewHolder {
        return DrinkViewHolder(
            ItemDrinkViewBinding.inflate(LayoutInflater.from(parent.context))
        )
    }

    override fun onBindViewHolder(holder: DrinksAdapter.DrinkViewHolder, position: Int) {
        with(holder) {
            drinkName.text = drinkList[position].strDrink
            displayImage(drinkList[position].strDrinkThumb)
            val elements = listOf(drinkName, drinkImg, wholeItem)
            for (element in elements) {
                element.setOnClickListener {
                    val action = DrinksFragmentDirections
                        .actionDrinksFragment2ToDrinkFragment(
                            drinkList[position].idDrink
                        )

                    Navigation.findNavController(holder.itemView)
                        .navigate(action)
                }
            }
        }
    }

    override fun getItemCount(): Int {
        return drinkList.size
    }

    fun setList(list: List<DrinkDetails>) {
        val oldSize = drinkList.size
        drinkList = list
        notifyItemRangeChanged(oldSize, drinkList.size)
    }

}