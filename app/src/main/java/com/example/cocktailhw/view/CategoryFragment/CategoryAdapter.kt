package com.example.cocktailhw.view.CategoryFragment

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import com.example.cocktailhw.R
import com.example.cocktailhw.model.models.Drink

class CategoryAdapter : RecyclerView.Adapter<CategoryAdapter.CategoryViewHolder>() {

    var categoryList: List<Drink> = listOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CategoryViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.item_category, parent, false)
        return CategoryViewHolder(v)
    }

    override fun onBindViewHolder(holder: CategoryViewHolder, position: Int) {
        with(holder.categoryName) {
            text = categoryList[position].strCategory
            setOnClickListener {
                val navController = Navigation.findNavController(holder.itemView)
                val action =
                    CategoryFragmentDirections.actionCategoryFragmentToDrinksFragment2(text.toString())
                navController.navigate(action)
            }
        }
    }

    override fun getItemCount(): Int {
        return categoryList.size
    }

    inner class CategoryViewHolder(categoryView: View) : RecyclerView.ViewHolder(categoryView) {
        var categoryName: TextView

        init {
            categoryName = categoryView.findViewById(R.id.tvCategoryItem)
        }
    }

    fun setData(list: List<Drink>) {
        categoryList = list
        notifyItemRangeChanged(0, categoryList.size)
    }
}