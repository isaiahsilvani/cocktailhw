package com.example.cocktailhw.view.CategoryFragment

import com.example.cocktailhw.model.models.Drink

data class CategoryState(
    val list: List<Drink> = listOf(),
    var isLoading: Boolean = false
)