package com.example.cocktailhw.view.CategoryFragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.cocktailhw.databinding.FragmentCategoryBinding
import com.example.cocktailhw.viewmodel.CategoryViewModel

class CategoryFragment : Fragment() {

    private val viewModel by viewModels<CategoryViewModel>()
    private val adapter by lazy {
        CategoryAdapter()
    }

    lateinit var binding: FragmentCategoryBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentCategoryBinding.inflate(layoutInflater)
        // Set up Recycler View
        with(binding) {
            rvCategories.adapter = adapter
            rvCategories.layoutManager = LinearLayoutManager(this@CategoryFragment.context)
        }
        // If categoriesState already loaded, don't make another API call
        viewModel.getCategories()
        initObservers()
        return binding.root
    }

    private fun initObservers() {
        viewModel.categoryState.observe(viewLifecycleOwner) { state ->
            if (state.isLoading) {
                binding.progressBar.isVisible = true
            } else if (!state.isLoading) {
                binding.progressBar.isVisible = false
            }
            adapter.setData(state.list)
        }
    }
}