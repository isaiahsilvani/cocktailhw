package com.example.cocktailhw.view.DrinkFragment

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import coil.load
import com.example.cocktailhw.R
import com.example.cocktailhw.databinding.FragmentDrinkBinding
import com.example.cocktailhw.model.models.Drink
import com.example.cocktailhw.viewmodel.DrinkViewModel

class DrinkFragment : Fragment() {

    private val args by navArgs<DrinkFragmentArgs>()
    private val viewModel by viewModels<DrinkViewModel>()
    lateinit var binding: FragmentDrinkBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentDrinkBinding.inflate(layoutInflater)
        viewModel.getDrinkDetails(args.drinkID)
        initObservers()
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        with(binding) {
            btnMainMenu.setOnClickListener {
                val action = DrinkFragmentDirections.actionDrinkFragmentToCategoryFragment()
                findNavController().navigate(action)
            }
        }
    }

    private fun initObservers() {
        with(binding) {
            viewModel.drinkState.observe(viewLifecycleOwner) { state ->
                with(state) {
                    // Loading.....
                    if (state.isLoading) {
                        binding.progressBar.isVisible = true
                    } else if (!state.isLoading) {
                        binding.progressBar.isVisible = false
                    }

                    tvDrinkName.text = drink?.strDrink
                    imgDrink.load(drink?.strDrinkThumb)
                    if (drink?.strInstructions?.isEmpty() == true) {
                        // todo ask your boys about this one
                        tvInstructions.text = context?.getString(R.string.no_instructions)
                    } else {
                        tvInstructions.text = drink?.strInstructions
                    }
                    // set the Go Back button to the state's category
                    btnGoBack.setOnClickListener {
                        val action =
                            DrinkFragmentDirections.actionDrinkFragmentToDrinksFragment2(drink!!.strCategory)
                        findNavController().navigate(action)
                    }
                }
            }
        }
    }
}