package com.example.cocktailhw.view.DrinksFragment

import com.example.cocktailhw.model.models.DrinkDetails

data class DrinksState(
    val drinks: List<DrinkDetails> = listOf(),
    val category: String? = null,
    var isLoading: Boolean = false
)