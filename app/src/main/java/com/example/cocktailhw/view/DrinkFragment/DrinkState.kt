package com.example.cocktailhw.view.DrinkFragment

import com.example.cocktailhw.model.models.DrinkX

data class DrinkState(
    val drink: DrinkX? = null,
    var isLoading: Boolean = false
)
