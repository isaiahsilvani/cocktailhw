package com.example.cocktailhw.model.models

data class Category(
    val drinks: List<Drink>
)