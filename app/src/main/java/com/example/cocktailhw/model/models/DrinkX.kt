package com.example.cocktailhw.model.models

data class DrinkX(
    val strAlcoholic: String,
    val strCategory: String,
    val strDrink: String,
    val strDrinkThumb: String,
    val strGlass: String,
    val strInstructions: String,
)