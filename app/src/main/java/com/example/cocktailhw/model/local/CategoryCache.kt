package com.example.cocktailhw.model.local

import com.example.cocktailhw.model.models.Drink

object CategoryCache {

    private var cache = mutableListOf<Drink>()


    fun updateCache(categoryList: List<Drink>) {
        cache.addAll(categoryList)
    }

    fun retrieveCache() = cache


}