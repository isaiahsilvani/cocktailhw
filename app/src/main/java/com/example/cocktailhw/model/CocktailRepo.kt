package com.example.cocktailhw.model

import android.util.Log
import com.example.cocktailhw.model.remote.CocktailService
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

object CocktailRepo {

    private val cocktailService = CocktailService.getInstance()

    suspend fun getCategories() = withContext(Dispatchers.IO) {
        return@withContext cocktailService.getCategories()
    }

    suspend fun getDrinks(category: String) = withContext(Dispatchers.IO) {
        Log.e("CockRepo", category)
        return@withContext cocktailService.getDrinks(category)
    }

    suspend fun getDrinkDetails(id: String) = withContext(Dispatchers.IO) {
        Log.e("TAGGG", "HIT BABY")
        return@withContext cocktailService.getDrinkDetails(id)
    }

}