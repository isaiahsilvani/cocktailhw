package com.example.cocktailhw.model.remote

import com.example.cocktailhw.model.models.Category
import com.example.cocktailhw.model.models.DrinkDetailsX
import com.example.cocktailhw.model.models.Drinks
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.create
import retrofit2.http.GET
import retrofit2.http.Query

interface CocktailService {

    companion object {
        private const val BASE_URL = "https://www.thecocktaildb.com"
        private const val CATEGORYLIST_ENDPOINT = "api/json/v1/1/list.php?c=list"
        private const val DRINKS_ENDPOINT = "/api/json/v1/1/filter.php"
        private const val DRINK_ENDPOINT = "/api/json/v1/1/lookup.php"

        fun getInstance(): CocktailService = Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create()
    }

    @GET(CATEGORYLIST_ENDPOINT)
    suspend fun getCategories(): Category

    @GET(DRINKS_ENDPOINT)
    suspend fun getDrinks(@Query("c") c: String): Drinks

    @GET(DRINK_ENDPOINT)
    suspend fun getDrinkDetails(@Query("i") i: String): DrinkDetailsX
}