package com.example.cocktailhw.model.models

data class Drinks(
    val drinks: List<DrinkDetails>
)