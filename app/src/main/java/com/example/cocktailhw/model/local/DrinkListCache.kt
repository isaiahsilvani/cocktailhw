package com.example.cocktailhw.model.local

import com.example.cocktailhw.model.models.DrinkDetails

object DrinkListCache {

    private var cache = mutableListOf<DrinkDetails>()
    private var lastCategory: String? = null

    fun updateCache(categoryList: List<DrinkDetails>) {
        cache.addAll(categoryList)
    }

    fun retrieveCache() = cache

    fun getLastCategory() = lastCategory

    fun setLastCategory(category: String) {
        lastCategory = category
    }

}