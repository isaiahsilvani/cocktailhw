package com.example.cocktailhw.model.models

data class DrinkDetails(
    val idDrink: String,
    val strDrink: String,
    val strDrinkThumb: String
)