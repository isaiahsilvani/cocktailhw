package com.example.cocktailhw.model.models

data class DrinkDetailsX(
    val drinks: List<DrinkX>
)