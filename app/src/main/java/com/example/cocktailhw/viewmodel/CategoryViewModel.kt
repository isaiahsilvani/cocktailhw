package com.example.cocktailhw.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.cocktailhw.model.CocktailRepo
import com.example.cocktailhw.model.local.CategoryCache
import com.example.cocktailhw.view.CategoryFragment.CategoryState
import kotlinx.coroutines.launch

const val TAG = "CategoryVM"

class CategoryViewModel : ViewModel() {
    private val categoryCache: CategoryCache = CategoryCache
    private val repo: CocktailRepo = CocktailRepo
    private val _categoryState: MutableLiveData<CategoryState> = MutableLiveData(CategoryState())
    val categoryState: LiveData<CategoryState> get() = _categoryState

    fun getCategories() {
        viewModelScope.launch {
            with(_categoryState) {
                val cache = categoryCache.retrieveCache()

                if (cache.isEmpty()) {
                    makeAPICall()
                } else {
                    value = CategoryState(list = cache, isLoading = false)
                }
            }
        }
    }

    private suspend fun makeAPICall() {
        with(_categoryState) {
            value = CategoryState(isLoading = true)
            val result = repo.getCategories()
            value = CategoryState(list = result.drinks, isLoading = false)
            categoryCache.updateCache(value!!.list)
        }
    }
}