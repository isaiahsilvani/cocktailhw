package com.example.cocktailhw.viewmodel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.cocktailhw.model.CocktailRepo
import com.example.cocktailhw.model.local.DrinkListCache
import com.example.cocktailhw.view.DrinksFragment.DrinksState
import kotlinx.coroutines.launch

class DrinksViewModel : ViewModel() {
    val TAG = "DrinksVM"
    private val cache = DrinkListCache
    private val repo = CocktailRepo
    private val _drinksState: MutableLiveData<DrinksState> = MutableLiveData(DrinksState())
    val drinksState: LiveData<DrinksState> get() = _drinksState

    fun getDrinks(category: String) {
        viewModelScope.launch {
            val drinksCache = cache.retrieveCache()
            val lastCategory = cache.getLastCategory()
            // If current category same as last category, get it from cache
            if (lastCategory != category) {
                Log.e(TAG, "CALL MADE")
                makeAPICall(category)
            } else {
                _drinksState.value = DrinksState(drinksCache)
            }
        }
    }

    private suspend fun makeAPICall(category: String) {
        with(_drinksState) {
            value = DrinksState(isLoading = true)
            val result = repo.getDrinks(category)
            value = DrinksState(drinks = result.drinks, isLoading = false)
            cache.updateCache(result.drinks)
            cache.setLastCategory(category)
        }
    }
}